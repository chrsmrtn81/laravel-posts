@extends('layouts.app')

@section('content')

    <div class="container w-25 bg-white p-4 mt-4">

            <form action="{{ route('register') }}" method="POST">

                @csrf
                <div class="form-group">
                    <input type="text" class="form-control @error('name') border border-danger @enderror" name="name" id="name" placeholder="Your name" value="{{ old('name') }}">
                    @error('name')
                    <div class="text-danger">
                        {{ $message }}
                      </div>
                    @enderror
                </div>

                <div class="form-group">
                    <input type="text" class="form-control @error('username') border border-danger @enderror" name="username" id="username" placeholder="Username" value="{{ old('username') }}">
                    @error('username')
                    <div class="text-danger">
                        {{ $message }}
                      </div>
                    @enderror
                </div>

                <div class="form-group">
                    <input type="text" class="form-control @error('email') border border-danger @enderror" name="email" id="email" placeholder="Your email" value="{{ old('email') }}">
                    @error('email')
                    <div class="text-danger">
                        {{ $message }}
                      </div>
                    @enderror
                </div>

                <div class="form-group">
                    <input type="password" class="form-control @error('password') border border-danger @enderror" name="password" id="password" placeholder="Choose a password" value="">
                    @error('password')
                    <div class="text-danger">
                        {{ $message }}
                      </div>
                    @enderror
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Repeat your password" value="">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary w-100">Register</button>
                </div>


            </form>

    </div>

@endsection
