@extends('layouts.app')

@section('content')

    <div class="container w-25 bg-white p-4 mt-4">

            @if (session('status'))
            <div class="text-danger">
                {{ session('status') }}
              </div>
            @endif            

            <form action="{{ route('login') }}" method="POST">

                @csrf

                <div class="form-group">
                    <input type="text" class="form-control @error('email') border border-danger @enderror" name="email" id="email" placeholder="Email" value="{{ old('email') }}">
                    @error('email')
                    <div class="text-danger">
                        {{ $message }}
                      </div>
                    @enderror
                </div>

                <div class="form-group">
                    <input type="password" class="form-control @error('password') border border-danger @enderror" name="password" id="password" placeholder="Password" value="">
                    @error('password')
                    <div class="text-danger">
                        {{ $message }}
                      </div>
                    @enderror
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary w-100">Login</button>
                </div>

                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="remember" id="remember">
                    <label class="custom-control-label" for="remember">Stay logged in</label>
                </div>

            </form>

    </div>

@endsection
