@extends('layouts.app')

@section('content')

    @auth
        <div class="container w-50 bg-white p-4 my-4">

            <form action="{{ route('posts') }}" method="POST">

                @csrf

                <div class="form-group">
                    <textarea class="form-control @error('body') border border-danger @enderror" name="body" id="body" placeholder="Post something!" value="{{ old('body') }}" rows="3"></textarea>
                    @error('body')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group mb-0">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>

        </div>
    @endauth

    <div class="container w-50 bg-white p-4 my-4">

        @if ($posts->count())
            @foreach ($posts as $post)
                <div class="mb-4">
                    <a class="font-weight-bold" href="#">{{ $post->user->name }}</a> <span class="text-secondary" >{{ $post->created_at->diffForHumans() }}</span>
                    <p>{{ $post->body }}</p>
                    <div class="d-flex">
                        @auth
                            @if (!$post->likedBy(auth()->user()))
                                <form action="{{ route('post.likes', $post) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn">Like</button>
                                </form>
                            @else
                                <form action="{{ route('post.likes', $post) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn">Unlike</button>
                                </form>
                            @endif

                            <form action="{{ route('posts.destroy', $post) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn">Delete</button>
                            </form>

                        @endauth
                        <span class="my-auto">{{$post->likes->count() }} {{ Str::plural('like', $post->likes->count()) }} </span>
                    </div>
                </div>
            @endforeach
        @else
            <p>There are no posts...</p>
        @endif

        {{ $posts->links() }}

    </div>

@endsection
