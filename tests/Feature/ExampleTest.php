<?php

namespace Tests\Feature;

use Faker\Factory as FakerFactory;
use Tests\TestCase;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    use RefreshDatabase;
    use Factory;
    
    /** @test */
    public function Guest_NoAccessToDashboard() :void
    {
        $response = $this->get('/dashboard')->assertRedirect('/login');
    }

    /** @test */
    public function User_AccessToDashboard() :void
    {
        $this->actingAs(factory(User::class)->create());

        $response = $this->get('/dashboard')->assertOk();
    }
    
}
